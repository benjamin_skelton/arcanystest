﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ScaleParticles : MonoBehaviour
{
    private ParticleSystem m_System;

    private void Awake()
    {
        m_System = GetComponent<ParticleSystem>();
    }
    void Update()
    {
        ParticleSystem.MainModule main = m_System.main;

        ParticleSystem.MinMaxCurve minMaxCurve = main.startSize; //Get Size

        minMaxCurve.constant *= transform.lossyScale.magnitude; //Modify Size
        main.startSize = minMaxCurve; //Assign the modified startSize back
    }
}