﻿using UnityEngine;
using UnityEngine.EventSystems;

public class AttackOnPressBehaviour : MonoBehaviour, IPointerDownHandler
{
    private PlayerInput playerInput;

    private void Awake()
    {
        playerInput = FindObjectOfType<PlayerInput>();
    }

    public void OnPointerDown(PointerEventData data)
    {
        playerInput.AttackQueued = true;
    }
}
