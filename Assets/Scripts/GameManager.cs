﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    protected static GameManager instance;
    public static GameManager Instance { get { return instance; } }
    public GameObject LostPanel;
    public GameObject WonPanel;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            throw new UnityException("There cannot be more than one GameManager.");
        }
        LostPanel.SetActive(false);
        WonPanel.SetActive(false);
    }

    public void Won()
    {
        Time.timeScale = 0f;
        WonPanel.SetActive(true);
    }

    public void Lost()
    {
        Time.timeScale = 0f;
        LostPanel.SetActive(true);
    }

    public void Retry()
    {
        StartCoroutine(reloadSceneAfterWait(1f));
    }

    private IEnumerator reloadSceneAfterWait(float waitTime)
    {
        yield return new WaitForSecondsRealtime(waitTime);
        Time.timeScale = 1f;
        SceneManager.LoadSceneAsync("StartMenu", LoadSceneMode.Single);
    }
}
