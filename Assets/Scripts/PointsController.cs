﻿using UnityEngine;
using TMPro;

public class PointsController : MonoBehaviour
{
    private int points = 0;
    private TMP_Text scoreDisplay;

    private void Start()
    {
        scoreDisplay = GetComponent<TMP_Text>();
    }

    public void ChangePoints(int change)
    {
        points = Mathf.Clamp(points + change, 0, 100);
        scoreDisplay.text = "Score: " + points.ToString();
        switch (points)
        {
            case 0:
                GameManager.Instance.Lost();
                break;
            case 100:
                GameManager.Instance.Won();
                break;
            default:
                break;
        }
    }

}
