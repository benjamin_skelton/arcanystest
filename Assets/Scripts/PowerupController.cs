﻿using UnityEngine;
using System.Collections;

public class PowerupController : MonoBehaviour
{
    public AudioClip PickUpSound;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AudioSource.PlayClipAtPoint(PickUpSound, transform.position);
            Lean.Pool.LeanPool.Despawn(gameObject);
            FindObjectOfType<PointsController>().ChangePoints(15);
            FindObjectOfType<PowerupSpawner>().SpawnPowerUp();
        }
    }
}
