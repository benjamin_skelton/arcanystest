﻿using UnityEngine;
using Lean.Pool;

public class PowerupSpawner : MonoBehaviour
{
    public GameObject[] PowerupPrefabs;
    public float MinX, MaxX, MinZ, MaxZ, Y;
    public float RadiusToCheck;
    public int InitialPowerupCount;

    private GameObject currentPowerup;

    private void Start()
    {
        for (int i = 0; i < InitialPowerupCount; i++)
        {
            SpawnPowerUp();
        }
    }

    public void SpawnPowerUp()
    {
        // if (currentPowerup)
        // {
        //     LeanPool.Despawn(currentPowerup);
        // }
        GameObject randomPowerup = PowerupPrefabs[Random.Range(0, PowerupPrefabs.Length)];
        int count = 0;
        Vector3 randPos = new Vector3(Random.Range(MinX, MaxX), Y, Random.Range(MinZ, MaxZ));
        do
        {
            randPos = new Vector3(Random.Range(MinX, MaxX), Y, Random.Range(MinZ, MaxZ));
            count++;
        } while (count < 100 && Physics.CheckSphere(randPos, RadiusToCheck));
        if (!Physics.CheckSphere(randPos, RadiusToCheck))
        {
            currentPowerup = LeanPool.Spawn(randomPowerup, randPos, Quaternion.identity);
            currentPowerup.transform.parent = transform;
        }
        else
        {
            Debug.Log("Couldn't place powerup after 100 tries");
        }
    }
}
