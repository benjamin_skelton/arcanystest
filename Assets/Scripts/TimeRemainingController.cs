﻿using UnityEngine;
using TMPro;

public class TimeRemainingController : MonoBehaviour
{
    private TMP_Text timeRemainingDisplay;
    public float timeRemaining = 90f;

    private void Start()
    {
        timeRemainingDisplay = GetComponent<TMP_Text>();
    }

    private void Update()
    {
        timeRemaining = Mathf.Clamp(timeRemaining - Time.deltaTime, 0f, 90f);
        timeRemainingDisplay.text = "Time: " + Mathf.Floor(timeRemaining).ToString();
        if (GameManager.Instance && timeRemaining == 0f)
        {
            GameManager.Instance.Lost();
        }
    }
}
